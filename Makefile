update:
	rm -rf node_modules/
	/opt/php73/bin/php -d memory_limit=1G /usr/local/bin/composer install --optimize-autoloader --no-dev --prefer-dist
	ln -f -s ./public/ public_html
