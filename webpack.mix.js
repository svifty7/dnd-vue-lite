const mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        modules: ['node_modules'],
        alias: {
            '@': path.resolve(__dirname, 'resources/'),
        }
    }
});

let productionSourceMaps = false;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sourceMaps(productionSourceMaps, 'source-map')
    .version();
mix.less('resources/less/app.less', 'public/css')
    .options({
        postCss: [
            require('autoprefixer'),
            require('cssnano')
        ]
    })
    .sourceMaps(productionSourceMaps, 'source-map')
    .version();

mix.copyDirectory('resources/assets/img', 'public/img');
mix.copyDirectory('resources/assets/json', 'public/json');

mix.browserSync({
    proxy: 'dnd-char.loc'
});

mix.disableNotifications();
