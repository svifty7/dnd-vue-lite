import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('../views/pages/home')
    },
    {
        path: '/char/:charId',
        component: () => import('../views/pages/character'),
        redirect: '/char/:charId/main',
        children: [
            {
                path: 'main',
                name: 'main-info',
                component: () => import('../views/components/character/main')
            },
            {
                path: 'extra-stats',
                name: 'extra-stats',
                component: () => import('../views/components/character/extra-stats')
            }
        ]
    },
    {
        path: '/about',
        name: 'about',
        component: () => import('../views/pages/about'),
        meta: {
            title: 'О нас'
        }
    },
    {
        path: '/404',
        name: 'Not Found',
        component: () => import('../views/pages/404'),
        meta: {
            title: 'Страница не найдена'
        }
    },
    {
        path: '*',
        redirect: '/404'
    }
];

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

