require('./bootstrap.js');
import Vue from 'vue'
import App from './views/layouts/app.vue'
import router from './router/router'
import store from './store/store'
import {VueMasonryPlugin} from 'vue-masonry'

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');

Vue.use(VueMasonryPlugin);

const siteName = 'DnD 5e Charsheet';

router.beforeEach((to, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title;
        next();
    } else {
        document.title = siteName;
        next();
    }
});
